import { Component, OnInit } from '@angular/core';

import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('English Butter Tarts','This is a recipe my mother had in Scotland. I have saved it since I was a teenager because the tarts are so good.',
      'http://media.foodnetwork.ca/recipetracker/dmm/P/E/Pecan_Butter_Tarts_001.jpg'),
    new Recipe('Mini pork pies with piccalilli','Hoel Levieil from London restaurant Frizzante shares this picnic-friendly pork pie recipe, which was voted our users’ favourite Jubilee recipe idea',
      'https://www.bbcgoodfood.com/sites/default/files/Collections_British_4.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
